package com.antonio.mywallet

import android.app.Application
import com.antonio.mywallet.di.ApplicationComponent
import com.antonio.mywallet.di.DaggerApplicationComponent

class WalletApp: Application() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory().onCreate(applicationContext)
    }
}