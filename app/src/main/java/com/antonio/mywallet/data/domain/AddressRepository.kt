package com.antonio.mywallet.data.domain

import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.Resource
import com.antonio.mywallet.data.local.AddressDao
import com.antonio.mywallet.data.server.RetrofitService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddressRepository @Inject constructor(
    private val addressDao: AddressDao,
    private val retrofitService: RetrofitService) {

    suspend fun generateAddress(): Resource<AddressEntity> {
        val response = retrofitService.generateAddress()
        return if(response.isSuccessful)
            Resource.success(response.body())
        else
            Resource.error(response.message(), null)
    }

    suspend fun saveAddress(address: AddressEntity) {
        addressDao.deleteAll()
        addressDao.insertAddress(address)
    }

    fun getAddress(): Flow<AddressEntity?> = addressDao.getAddress()
}