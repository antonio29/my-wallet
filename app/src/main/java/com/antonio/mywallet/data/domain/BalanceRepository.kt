package com.antonio.mywallet.data.domain

import com.antonio.mywallet.data.entities.BalanceEntity
import com.antonio.mywallet.data.entities.Resource
import com.antonio.mywallet.data.local.BalanceDao
import com.antonio.mywallet.data.server.RetrofitService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BalanceRepository @Inject constructor(
    private val balanceDao: BalanceDao,
    private val retrofitService: RetrofitService) {

    fun getBalanceFromDataBase(): Flow<BalanceEntity> = balanceDao.getBalance()

    suspend fun getBalanceFromService(address: String): Resource<BalanceEntity> {
        val response = retrofitService.getBalance(address)
        return if(response.isSuccessful)
            Resource.success(response.body()!!)
        else
            Resource.error(response.message(), null)
    }

    suspend fun saveBalance(balance: BalanceEntity) {
        balanceDao.deleteAll()
        balanceDao.insertBalance(balance)
    }

}