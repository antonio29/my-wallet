package com.antonio.mywallet.data.domain

import com.antonio.mywallet.data.entities.HistoryResponse
import com.antonio.mywallet.data.entities.Resource
import com.antonio.mywallet.data.server.RetrofitService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HistoryRepository @Inject constructor(private val retrofitService: RetrofitService) {
    suspend fun getHistory(address: String): Resource<HistoryResponse> {
        retrofitService.getHistory(address).also { response ->
            return if(response.isSuccessful)
                Resource.success(response.body())
            else
                Resource.error(response.message(), null)
        }
    }
}