package com.antonio.mywallet.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address")
data class AddressEntity(
    @PrimaryKey
    var address: String,
    var private: String,
    var public: String,
    var wif: String
) {
    constructor(): this("", "", "", "")
}