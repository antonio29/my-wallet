package com.antonio.mywallet.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "balance")
data class BalanceEntity (
    @PrimaryKey
    var address: String,
    @SerializedName(value = "balance")
    var balance: String,
    @SerializedName(value = "unconfirmed_balance")
    var unconfirmedBalance: String,
    @SerializedName(value = "final_balance")
    var finalBalance: String
)