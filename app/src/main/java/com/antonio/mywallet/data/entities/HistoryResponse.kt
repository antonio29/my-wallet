package com.antonio.mywallet.data.entities

data class HistoryResponse(
    val address: String,
    val txs: List<TxsObject>
)

data class TxsObject(
    val total: Long,
    val confirmed: String,
    val received: String
)