package com.antonio.mywallet.data.entities

enum class Status {
    SUCCESS,
    ERROR
}