package com.antonio.mywallet.data.local

import androidx.room.*
import com.antonio.mywallet.data.entities.AddressEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface AddressDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAddress(address: AddressEntity)

    @Query("SELECT * FROM address LIMIT 1")
    fun getAddress(): Flow<AddressEntity?>

    @Query("DELETE FROM address")
    suspend fun deleteAll()
}