package com.antonio.mywallet.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.BalanceEntity

@Database(entities = [AddressEntity::class, BalanceEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {

    companion object {
        const val DATABASE_NAME = "db-app"
    }

    abstract fun addressDao(): AddressDao
    abstract fun balanceDao(): BalanceDao
}