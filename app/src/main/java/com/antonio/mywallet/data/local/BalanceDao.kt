package com.antonio.mywallet.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.antonio.mywallet.data.entities.BalanceEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BalanceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBalance(balance: BalanceEntity)

    @Query("SELECT * FROM balance LIMIT 1")
    fun getBalance(): Flow<BalanceEntity>

    @Query("DELETE FROM balance")
    suspend fun deleteAll()
}