package com.antonio.mywallet.data.server

import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.BalanceEntity
import com.antonio.mywallet.data.entities.HistoryResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface RetrofitService {

    @POST("addrs")
    suspend fun generateAddress(): Response<AddressEntity>

    @GET("addrs/{address}/full")
    suspend fun getHistory(@Path("address") addres: String): Response<HistoryResponse>

    @GET("addrs/{address}/balance")
    suspend fun getBalance(@Path("address") addres: String): Response<BalanceEntity>
}