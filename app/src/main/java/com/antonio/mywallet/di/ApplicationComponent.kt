package com.antonio.mywallet.di

import android.content.Context
import com.antonio.mywallet.data.server.NetworkModule
import com.antonio.mywallet.ui.address.di.AddressComponent
import com.antonio.mywallet.ui.transaction_history.di.TransactionHistoryComponent
import com.antonio.mywallet.ui.wallet_state.di.WalletStateComponent
import com.antonio.mywallet.utils.UpdateBalanceService
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

// Dagger will use ApplicationComponent.class to know how to create objects
// And those objets will be accessible though ApplicationComponent
@Singleton
@Component(modules = [ApplicationModule::class, SubComponents::class, NetworkModule::class])
interface ApplicationComponent {

    fun inject(updateBalanceService: UpdateBalanceService)

    fun getWalletStateComponent(): WalletStateComponent.Factory

    fun getTransactionHistoryComponent(): TransactionHistoryComponent.Factory

    fun getAddressComponent(): AddressComponent.Factory

    // Context is needed to create instances of AppDatabase
    @Component.Factory
    interface Factory {
        fun onCreate(@BindsInstance applicationContext: Context): ApplicationComponent
    }
}