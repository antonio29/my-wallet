package com.antonio.mywallet.di

import android.content.Context
import androidx.room.Room
import com.antonio.mywallet.data.local.AddressDao
import com.antonio.mywallet.data.local.AppDatabase
import com.antonio.mywallet.data.local.BalanceDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

    @Singleton
    @Provides
    fun provideAddressDao(applicationContext: Context): AddressDao {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
            .addressDao()
    }

    @Singleton
    @Provides
    fun provideBalanceDao(applicationContext: Context): BalanceDao {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
            .balanceDao()
    }
}