package com.antonio.mywallet.di

import com.antonio.mywallet.ui.address.di.AddressComponent
import com.antonio.mywallet.ui.transaction_history.di.TransactionHistoryComponent
import com.antonio.mywallet.ui.wallet_state.di.WalletStateComponent
import dagger.Module

@Module(subcomponents = [WalletStateComponent::class, TransactionHistoryComponent::class, AddressComponent::class])
class SubComponents {
}