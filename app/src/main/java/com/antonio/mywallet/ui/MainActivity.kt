package com.antonio.mywallet.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.antonio.mywallet.R
import com.antonio.mywallet.databinding.ActivityMainBinding
import com.antonio.mywallet.ui.address.AddressFragment
import com.antonio.mywallet.ui.transaction_history.TransactionHistoryFragment
import com.antonio.mywallet.ui.wallet_state.WalletStateFragment
import com.antonio.mywallet.utils.UpdateBalanceService

class MainActivity : AppCompatActivity(),
    AddressFragment.OnFragmentInteractionListener,
    WalletStateFragment.OnFragmentInteractionListener,
    TransactionHistoryFragment.OnFragmentInteractionListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_wallet, R.id.navigation_history, R.id.navigation_address
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.setupWithNavController(navController)

        Intent(this, UpdateBalanceService::class.java).also { intent ->
            startService(intent)
        }
    }

    private fun goToWalletView() {
        val walletView = binding.navView.findViewById<View>(R.id.navigation_wallet)
        walletView.performClick()
    }

    override fun goToWalletFromAddress() {
        goToWalletView()
    }

    override fun goToAddressFromWalletState() {
        navController.navigate(R.id.navigation_address)
    }

    override fun goToAddressFromTransactionHistory() {
        navController.navigateUp()
        navController.navigate(R.id.navigation_address)
    }

    override fun goToWalletFromTransactionHistory() {
        goToWalletView()
    }

    override fun onDestroy() {
        stopService(Intent(this, UpdateBalanceService::class.java))
        super.onDestroy()
    }
}