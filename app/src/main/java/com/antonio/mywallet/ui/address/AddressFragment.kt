package com.antonio.mywallet.ui.address

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.antonio.mywallet.R
import com.antonio.mywallet.WalletApp
import com.antonio.mywallet.databinding.FragmentAddressBinding
import com.antonio.mywallet.ui.MainActivity
import com.antonio.mywallet.ui.wallet_state.WalletStateFragment
import com.antonio.mywallet.utils.Utils
import com.google.zxing.WriterException
import javax.inject.Inject

class AddressFragment : Fragment() {

    private var _binding: FragmentAddressBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var viewModel: AddressViewModel
    private var onFragmentInteractionListener: OnFragmentInteractionListener? = null
    private lateinit var mContext: Context

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentAddressBinding.inflate(inflater, container, false)
        mContext = requireContext()

        initObservers()

        binding.btnGenerate.setOnClickListener {
            viewModel.generateAddress(mContext)
        }

        binding.btnSave.setOnClickListener {
            viewModel.address.value?.let {
                Utils.makeAttentionDialog(mContext, mContext.resources.getString(R.string.msg_want_to_save_address))
                    .setPositiveButton(mContext.resources.getString(R.string.action_yes)) { dialog, _ ->
                        dialog.dismiss()
                        viewModel.saveAddress(mContext)
                    }
                    .setNegativeButton(mContext.resources.getString(R.string.action_no)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            } ?:
                Utils.makeAttentionDialog(mContext, mContext.resources.getString(R.string.msg_generate_address))
                    .setPositiveButton(mContext.resources.getString(R.string.action_accept)) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
        }

        return binding.root
    }

    private fun initObservers() {
        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if(it)
                binding.progressBarContent.visibility = View.VISIBLE
            else
                binding.progressBarContent.visibility = View.GONE
        })

        viewModel.address.observe(viewLifecycleOwner, Observer {
            binding.textHome.text = it.address
            val encoder = QRGEncoder(it.address, null, QRGContents.Type.TEXT, 500)
            try {
                binding.qrView.setImageBitmap(encoder.bitmap)
            } catch (e: WriterException) {
                Log.e(WalletStateFragment.TAG, e.toString())
            }
        })

        viewModel.goToWalletFromAddress.observe(viewLifecycleOwner, Observer {
            onFragmentInteractionListener?.let { listener ->
                if(it)
                    Utils.makeSuccessDialog(mContext, mContext.resources.getString(R.string.msg_address_saved))
                        .setPositiveButton(mContext.resources.getString(R.string.action_accept)){ dialog, _ ->
                            dialog.dismiss()
                            listener.goToWalletFromAddress()
                        }
                        .create()
                        .show()
            }
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Utils.makeAttentionDialog(mContext, it)
                .setPositiveButton(mContext.resources.getString(R.string.action_accept)){ dialog, _ ->
                    dialog.dismiss()
                }
                .setCancelable(false)
                .create()
                .show()
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = activity as MainActivity
        (activity.application as WalletApp).applicationComponent
            .getAddressComponent()
            .create(this)
            .inject(this)

        if(context is OnFragmentInteractionListener)
            onFragmentInteractionListener = context
        else
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
    }

    interface OnFragmentInteractionListener {
        fun goToWalletFromAddress()
    }
}