package com.antonio.mywallet.ui.address

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.antonio.mywallet.R
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.Status
import com.antonio.mywallet.utils.Utils
import kotlinx.coroutines.launch

class AddressViewModel constructor(private val repository: AddressRepository): ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _goToWalletFromAddress = MutableLiveData<Boolean>()
    val goToWalletFromAddress: LiveData<Boolean> = _goToWalletFromAddress
    private val _address = MutableLiveData<AddressEntity>()
    val address: LiveData<AddressEntity> = _address
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    fun generateAddress(context: Context) {
        if(Utils.isNetworkAvailable(context))
            viewModelScope.launch {
                _isLoading.value = true
                val newAddress = repository.generateAddress()
                when(newAddress.status) {
                    Status.SUCCESS -> _address.postValue(newAddress.data!!)
                    Status.ERROR -> _errorMessage.postValue(newAddress.message!!)
                }

                _isLoading.postValue(false)
            }
        else
            _errorMessage.value = context.resources.getString(R.string.msg_no_connection)
    }

    fun saveAddress(context: Context) {
        _address.value?.let {
            viewModelScope.launch {
                _isLoading.postValue(true)

                repository.saveAddress(it)

                _isLoading.postValue(false)
                _goToWalletFromAddress.postValue(true)
            }
        } ?:
            _errorMessage.postValue(context.resources.getString(R.string.msg_generate_address))
    }
}