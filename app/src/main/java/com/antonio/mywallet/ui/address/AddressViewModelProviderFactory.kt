package com.antonio.mywallet.ui.address

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository

class AddressViewModelProviderFactory
    constructor(private val repository: AddressRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(AddressViewModel::class.java))
            return AddressViewModel(repository) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}