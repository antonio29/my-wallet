package com.antonio.mywallet.ui.address.di

import com.antonio.mywallet.ui.address.AddressFragment
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [AddressModule::class])
interface AddressComponent {

    fun inject(addressFragment: AddressFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance addressFragment: AddressFragment): AddressComponent
    }
}