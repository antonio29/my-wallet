package com.antonio.mywallet.ui.address.di

import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.ui.address.AddressFragment
import com.antonio.mywallet.ui.address.AddressViewModel
import com.antonio.mywallet.ui.address.AddressViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class AddressModule {

    @Provides
    fun provideAddressVideModel(
        fragment: AddressFragment,
        addressRepository: AddressRepository
    ): AddressViewModel {
        val factory = AddressViewModelProviderFactory(addressRepository)
        return ViewModelProvider(fragment, factory).get(AddressViewModel::class.java)
    }
}