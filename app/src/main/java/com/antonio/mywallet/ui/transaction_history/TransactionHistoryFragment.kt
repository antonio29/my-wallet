package com.antonio.mywallet.ui.transaction_history

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.antonio.mywallet.R
import com.antonio.mywallet.WalletApp
import com.antonio.mywallet.data.entities.TxsObject
import com.antonio.mywallet.databinding.FragmentTransactionHistoryBinding
import com.antonio.mywallet.ui.MainActivity
import com.antonio.mywallet.ui.transaction_history.adapter.HistoryAdapter
import com.antonio.mywallet.utils.Utils
import javax.inject.Inject

class TransactionHistoryFragment : Fragment() {

    private var _binding: FragmentTransactionHistoryBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var viewModel: TransactionHistoryViewModel

    private var onFragmentInteractionListener: OnFragmentInteractionListener? = null
    private lateinit var historyList: ArrayList<TxsObject>
    private lateinit var mContext: Context

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionHistoryBinding.inflate(inflater, container, false)
        mContext = requireContext()

        historyList = ArrayList()
        binding.recyclerHistory.apply {
            adapter = HistoryAdapter(historyList)
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
        }
        binding.refreshHistory.setOnRefreshListener {
            viewModel.getHistory()
        }

        initObservers()

        return binding.root
    }

    fun initObservers() {

        viewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.refreshHistory.isRefreshing = it
        })

        viewModel.address.observe(viewLifecycleOwner, Observer {
            it?.let {
                if(Utils.isNetworkAvailable(mContext))
                    viewModel.getHistory()
                else
                    Utils.makeAttentionDialog(mContext, mContext.resources.getString(R.string.msg_no_connection))
                        .setPositiveButton(mContext.resources.getString(R.string.action_accept)) { dialog, _ ->
                            onFragmentInteractionListener?.goToWalletFromTransactionHistory()
                            dialog.dismiss()
                        }
                        .create()
                        .show()
            } ?:
                Utils.makeAttentionDialog(mContext, mContext.resources.getString(R.string.msg_no_address))
                .setPositiveButton(mContext.resources.getString(R.string.action_accept)) { dialog, _ ->
                    onFragmentInteractionListener?.goToAddressFromTransactionHistory()
                    dialog.dismiss()
                }
                .create()
                .show()
        })

        viewModel.history.observe(viewLifecycleOwner, Observer {
            if(it.txs.isNotEmpty()) {
                historyList.clear()
                historyList.addAll(it.txs)
                binding.recyclerHistory.adapter?.notifyDataSetChanged()
            }
            else
                Utils.makeAttentionDialog(mContext, mContext.resources.getString(R.string.msg_no_history))
                    .setPositiveButton(mContext.resources.getString(R.string.action_accept)){ dialog, _ ->
                        onFragmentInteractionListener?.goToWalletFromTransactionHistory()
                        dialog.dismiss()
                    }
                    .setCancelable(false)
                    .create()
                    .show()
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Utils.makeErrorDialog(mContext, it)
                .setPositiveButton(mContext.resources.getString(R.string.action_accept)){ dialog, _ ->
                    dialog.dismiss()
                }
                .setCancelable(false)
                .create()
                .show()
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = (activity as MainActivity)
        (activity.application as WalletApp).applicationComponent
            .getTransactionHistoryComponent()
            .create(this)
            .inject(this)

        if(context is OnFragmentInteractionListener)
            onFragmentInteractionListener = context
        else
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
    }

    interface OnFragmentInteractionListener {
        fun goToAddressFromTransactionHistory()
        fun goToWalletFromTransactionHistory()
    }
}