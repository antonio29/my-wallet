package com.antonio.mywallet.ui.transaction_history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.HistoryRepository
import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.HistoryResponse
import com.antonio.mywallet.data.entities.Status
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class TransactionHistoryViewModel
    constructor(private val historyRepository: HistoryRepository,
                private val addressRepository: AddressRepository) : ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading
    private val _address = MutableLiveData<AddressEntity?>()
    val address: LiveData<AddressEntity?> = _address
    private val _history = MutableLiveData<HistoryResponse>()
    val history: LiveData<HistoryResponse> = _history
    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    init {
        viewModelScope.launch {
            addressRepository.getAddress().collect {
                _address.value = it
            }
        }
    }

    fun getHistory(){
        viewModelScope.launch {
            _address.value?.let {
                _isLoading.postValue(true)

                val historyResponse = historyRepository.getHistory(it.address)
                when(historyResponse.status){
                    Status.SUCCESS -> _history.value = historyResponse.data!!
                    Status.ERROR -> _errorMessage.value = historyResponse.message!!
                }

                _isLoading.postValue(false)
            }
        }
    }
}