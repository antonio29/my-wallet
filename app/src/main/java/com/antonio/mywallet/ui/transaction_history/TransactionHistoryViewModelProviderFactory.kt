package com.antonio.mywallet.ui.transaction_history

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.HistoryRepository

class TransactionHistoryViewModelProviderFactory
    constructor(private val historyRepository: HistoryRepository,
                private val addressRepository: AddressRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(TransactionHistoryViewModel::class.java))
            return TransactionHistoryViewModel(historyRepository, addressRepository) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}