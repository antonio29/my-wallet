package com.antonio.mywallet.ui.transaction_history.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.antonio.mywallet.data.entities.TxsObject

class HistoryAdapter(private val dataSet: List<TxsObject>): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_2, parent, false)

        return ViewHolder(view)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val textTotal: TextView
        val textDate: TextView

        init {
            view.apply {
                textTotal = findViewById(android.R.id.text1)
                textDate = findViewById(android.R.id.text2)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataSet[position]
        holder.textTotal.text = "Total: ${item.total}"
        holder.textDate.text = "Fecha: ${item.received}"
    }

    override fun getItemCount(): Int = dataSet.size
}