package com.antonio.mywallet.ui.transaction_history.di

import com.antonio.mywallet.ui.transaction_history.TransactionHistoryFragment
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [TransactionHistoryModule::class])
interface TransactionHistoryComponent {

    fun inject(transactionHistoryFragment: TransactionHistoryFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance fragment: TransactionHistoryFragment): TransactionHistoryComponent
    }
}