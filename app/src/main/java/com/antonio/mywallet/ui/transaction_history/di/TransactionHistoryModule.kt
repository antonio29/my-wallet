package com.antonio.mywallet.ui.transaction_history.di

import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.HistoryRepository
import com.antonio.mywallet.ui.transaction_history.TransactionHistoryFragment
import com.antonio.mywallet.ui.transaction_history.TransactionHistoryViewModel
import com.antonio.mywallet.ui.transaction_history.TransactionHistoryViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class TransactionHistoryModule {

    @Provides
    fun provideTransactionHistoryViewModel(
        fragment: TransactionHistoryFragment,
        historyRepository: HistoryRepository,
        addressRepository: AddressRepository
    ): TransactionHistoryViewModel {
        val factory = TransactionHistoryViewModelProviderFactory(historyRepository, addressRepository)
        return ViewModelProvider(fragment, factory).get(TransactionHistoryViewModel::class.java)
    }
}