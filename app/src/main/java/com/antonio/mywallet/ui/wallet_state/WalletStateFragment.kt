package com.antonio.mywallet.ui.wallet_state

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.antonio.mywallet.R
import com.antonio.mywallet.WalletApp
import com.antonio.mywallet.databinding.FragmentWalletStateBinding
import com.antonio.mywallet.ui.MainActivity
import com.antonio.mywallet.utils.Utils
import com.google.zxing.WriterException
import javax.inject.Inject

class WalletStateFragment : Fragment() {

    companion object {
        const val TAG = "WalletStateFragment"
    }

    private var _binding: FragmentWalletStateBinding? = null
    private val binding get() = _binding!!
    @Inject
    lateinit var viewModel: WalletStateViewModel
    private var onFragmentInteractionListener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentWalletStateBinding.inflate(inflater, container, false)

        initObservers()

        return binding.root
    }

    private fun initObservers() {

        viewModel.address.observe(viewLifecycleOwner, Observer {
            it?.let { address ->
                binding.textAddress.text = address.address
                val encoder = QRGEncoder(address.address, null, QRGContents.Type.TEXT, 500)
                try {
                    binding.qrWalletView.setImageBitmap(encoder.bitmap)
                } catch (e: WriterException) {
                    Log.e(TAG, e.toString())
                }
            } ?: onFragmentInteractionListener?.goToAddressFromWalletState()
        })

        viewModel.balance.observe(viewLifecycleOwner, Observer {
            it?.let { balance ->
                binding.textBalanceWallet.text = balance.finalBalance
                binding.textUnconfirmedBalanceWallet.text = balance.unconfirmedBalance
                binding.textFinalBalanceWallet.text = balance.finalBalance
            }
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Utils.makeAttentionDialog(requireContext(), it)
                .setPositiveButton(requireContext().resources.getString(R.string.action_accept)){ dialog, _ ->
                    dialog.dismiss()
                }
                .setCancelable(false)
                .create()
                .show()
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        val activity = (activity as MainActivity)
        (activity.application as WalletApp).applicationComponent
            .getWalletStateComponent()
            .create(this)
            .inject(this)

        if(context is OnFragmentInteractionListener)
            onFragmentInteractionListener = context
        else
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
    }

    interface OnFragmentInteractionListener {
        fun goToAddressFromWalletState()
    }
}