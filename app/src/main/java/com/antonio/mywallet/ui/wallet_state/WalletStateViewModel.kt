package com.antonio.mywallet.ui.wallet_state

import android.content.Context
import androidx.lifecycle.*
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.BalanceRepository
import com.antonio.mywallet.data.entities.AddressEntity
import com.antonio.mywallet.data.entities.BalanceEntity
import com.antonio.mywallet.data.entities.Status
import com.antonio.mywallet.utils.Utils
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class WalletStateViewModel
        constructor(private val addressRepository: AddressRepository,
                    private val balanceRepository: BalanceRepository,
                    private val context: Context) : ViewModel() {

        private val _address = MutableLiveData<AddressEntity?>()
        val address: LiveData<AddressEntity?> = _address
        private val _balance = MutableLiveData<BalanceEntity>()
        val balance: LiveData<BalanceEntity> = _balance
        private val _errorMessage = MutableLiveData<String>()
        val errorMessage: LiveData<String> = _errorMessage

        init {
                getAddress()
                getBalance()
        }

        private fun getBalance() {
                viewModelScope.launch {
                        balanceRepository.getBalanceFromDataBase().collect {
                                _balance.value = it
                        }
                }
        }

        private fun getAddress() {
                viewModelScope.launch {
                        addressRepository.getAddress().collect {
                                _address.value = it
                                it?.address?.let { address ->
                                        if(Utils.isNetworkAvailable(context)) {
                                                val newBalance = balanceRepository.getBalanceFromService(address)
                                                when(newBalance.status) {
                                                        Status.SUCCESS -> balanceRepository.saveBalance(newBalance.data!!)
                                                        Status.ERROR -> _errorMessage.postValue(newBalance.message!!)
                                                }
                                        }
                                }
                        }
                }
        }
}