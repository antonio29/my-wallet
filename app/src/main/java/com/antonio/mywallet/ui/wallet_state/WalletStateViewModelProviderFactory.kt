package com.antonio.mywallet.ui.wallet_state

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.BalanceRepository

class WalletStateViewModelProviderFactory
    constructor(private val addressRepository: AddressRepository,
                private val balanceRepository: BalanceRepository,
                private val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(WalletStateViewModel::class.java))
            return WalletStateViewModel(addressRepository, balanceRepository, context) as T

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}