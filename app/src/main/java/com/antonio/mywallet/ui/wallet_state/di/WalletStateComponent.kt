package com.antonio.mywallet.ui.wallet_state.di

import com.antonio.mywallet.ui.wallet_state.WalletStateFragment
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [WalletStateModule::class])
interface WalletStateComponent {

    fun inject(walletStateFragment: WalletStateFragment)

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance fragment: WalletStateFragment): WalletStateComponent
    }

}