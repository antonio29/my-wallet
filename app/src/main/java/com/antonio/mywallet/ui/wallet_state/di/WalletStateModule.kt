package com.antonio.mywallet.ui.wallet_state.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.BalanceRepository
import com.antonio.mywallet.ui.wallet_state.WalletStateFragment
import com.antonio.mywallet.ui.wallet_state.WalletStateViewModel
import com.antonio.mywallet.ui.wallet_state.WalletStateViewModelProviderFactory
import dagger.Module
import dagger.Provides

@Module
class WalletStateModule {

    @Provides
    fun provideWalletViewModel(
        fragment: WalletStateFragment,
        addressRepository: AddressRepository,
        balanceRepository: BalanceRepository,
        context: Context
    ): WalletStateViewModel {
        val factory = WalletStateViewModelProviderFactory(addressRepository, balanceRepository, context)
        return ViewModelProvider(fragment, factory).get(WalletStateViewModel::class.java)
    }
}