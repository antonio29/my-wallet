package com.antonio.mywallet.utils

import android.app.IntentService
import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.antonio.mywallet.WalletApp
import com.antonio.mywallet.data.domain.AddressRepository
import com.antonio.mywallet.data.domain.BalanceRepository
import com.antonio.mywallet.data.entities.Status
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.util.*
import javax.inject.Inject

class UpdateBalanceService: Service() {

    companion object {
        const val TAG = "UpdateBalanceService"
    }

    private lateinit var timer: Timer
    @Inject
    lateinit var addressRepository: AddressRepository
    @Inject
    lateinit var balanceRepository: BalanceRepository

    override fun onCreate() {
        super.onCreate()
        (application as WalletApp).applicationComponent.inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        timer = Timer()
        timer.schedule(
            object: TimerTask() {
                override fun run() {
                    GlobalScope.launch(Dispatchers.IO) {
                        addressRepository.getAddress().collect { address ->
                            address?.let {
                                if(Utils.isNetworkAvailable(applicationContext)) {
                                    val newBalance = balanceRepository.getBalanceFromService(it.address)
                                    when(newBalance.status) {
                                        Status.SUCCESS -> balanceRepository.saveBalance(newBalance.data!!)
                                        Status.ERROR -> Log.e(TAG, newBalance.message!!)
                                    }
                                }
                            }
                        }
                    }
                }
            },
        0,
        10000)

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        timer.cancel()
        super.onDestroy()
    }
}