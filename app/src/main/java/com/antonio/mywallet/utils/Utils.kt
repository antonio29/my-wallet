package com.antonio.mywallet.utils

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AlertDialog
import com.antonio.mywallet.R

class Utils {
    companion object {
        fun makeAttentionDialog(mContext: Context, message: String): AlertDialog.Builder = AlertDialog.Builder(mContext)
            .setTitle(mContext.resources.getString(R.string.title_attention))
            .setMessage(message)

        fun makeSuccessDialog(mContext: Context, message: String): AlertDialog.Builder = AlertDialog.Builder(mContext)
            .setTitle(mContext.resources.getString(R.string.title_success))
            .setMessage(message)

        fun makeErrorDialog(mContext: Context, message: String): AlertDialog.Builder = AlertDialog.Builder(mContext)
            .setTitle(mContext.resources.getString(R.string.title_error))
            .setMessage(message)

        fun isNetworkAvailable(context: Context): Boolean {
            val conManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val internetInfo = conManager.activeNetworkInfo
            return internetInfo != null && internetInfo.isConnected
        }
    }
}